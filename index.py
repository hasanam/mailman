from flask import Flask, Response
from flask import request
from zipfile import ZipFile
from flask import jsonify
from dateutil import parser
from utils import utils
import datetime
import traceback
from dateutil import tz
import easyimap
from utils import mail
import imaplib
app = Flask(__name__)


def get_text(msg):
    if msg.is_multipart():
        return get_text(msg.get_payload(0))
    else:
        return msg.get_payload(None, True).decode('utf-8')


def get_imap(domain):
    with ZipFile('imap.zip') as myzip:
        with myzip.open('IMAP.txt') as myfile:
            imaps = myfile.read().decode('UTF-8')
            for line in imaps.splitlines():
                if line.split(":")[0] == domain:
                    return (line.split(":")[1], line.split(":")[2])


@app.route('/mail')
def inbox():
    email = request.args.get("email", default=None)
    if not email:
        email = request.args.get("username", default=None)
    if not email or not len(email):
        return {"error": "need params"}
    password = request.args.get("password", default=None)
    count = request.args.get('count', default=2, type=int)
    folders = request.args.get('folder', default=None, type=str)
    if folders:
        if "," in folders:
            folders = folders.split(",")
            if "INBOX" in folders:
                folders.remove("INBOX")
        else:
            folders = [folders]
    proxy = request.args.get("proxy", default=None)
    print("email", email, "password", password, "proxy", proxy)
    # proxy = "socks5://mrmah3323:fafaab@213.108.196.254:10702"
    try:
        mails = mail.get_latest_mails(email, password, count, proxy=proxy)
        print("Mails", mails)
        return {'mails': mails}
    except imaplib.IMAP4.error as err:
        traceback.print_exception(None, err, err.__traceback__)
        return {'error': 'unauthorized'}

if __name__ == '__main__':
    app.run(host="0.0.0.0")
# @app.route('/')
# def catch_all():
#     return Response("<h1>Flask</h1><p>You visited: /%s</p>" % (path), mimetype="text/html")
