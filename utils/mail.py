from utils.socks_imap import *
from utils import utils
import urllib


def check_mail(username, password, count=1, proxy=None):
    domain = username.split("@")[1]
    imap_info = utils.get_imap(domain)
    print(imap_info)
    if proxy:
        parsed = urllib.parse.urlparse(proxy)
        mail = SocksIMAP4SSL(host=imap_info[0], port=int(
            imap_info[1]), proxy_addr=parsed.hostname, proxy_port=parsed.port, username=parsed.username, password=parsed.password)
    else:
        mail = IMAP4_SSL(imap_info[0], port=int(imap_info[1]))

    rv, data = mail.login(username, password)


def get_latest_mails(username, password, count=1, proxy=None):
    domain = username.split("@")[1]
    imap_info = utils.get_imap(domain)
    print(imap_info)
    if proxy:
        parsed = urllib.parse.urlparse(proxy)
        mail = SocksIMAP4SSL(host=imap_info[0], port=int(
            imap_info[1]), proxy_addr=parsed.hostname, proxy_port=parsed.port, username=parsed.username, password=parsed.password)
    else:
        mail = IMAP4_SSL(imap_info[0], port=int(imap_info[1]))

    rv, data = mail.login(username, password)

    selected = mail.select('INBOX')
    messageCount = int(selected[1][0])
    print("messageCount", messageCount)
    res = []
    for i in range(messageCount, messageCount - count, -1):
        latest_email_uid = str(i)
        print("latest_email_uid", latest_email_uid)
        try:
            status, email_data = mail.fetch(latest_email_uid, '(UID RFC822)')
        except:
            continue
        # result, email_data = conn.store(num,'-FLAGS','\\Seen')
        # this might work to set flag to seen, if it doesn't already
        print('email_data', email_data)
        raw_email = email_data[0][1]
        raw_email_string = raw_email.decode('utf-8')
        email_message = email.message_from_string(raw_email_string)

        if email_message.is_multipart():
            mail_content = ''
            for part in email_message.get_payload():
                if part.get_content_type() == 'text/plain':
                    mail_content += part.get_payload()
        else:
            mail_content = email_message.get_payload()

        # Header Details
        date_tuple = email.utils.parsedate_tz(email_message['Date'])
        if date_tuple:
            local_date = datetime.datetime.fromtimestamp(
                email.utils.mktime_tz(date_tuple))
            local_message_date = "%s" % (
                str(local_date.strftime("%a, %d %b %Y %H:%M:%S")))
        email_from = str(email.header.make_header(
            email.header.decode_header(email_message['From'])))
        email_to = str(email.header.make_header(
            email.header.decode_header(email_message['To'])))
        subject = str(email.header.make_header(
            email.header.decode_header(email_message['Subject'])))
        res.append({
            "uid": latest_email_uid,
            "title": subject,
            "body": mail_content,
            "timstamp": parser.parse(local_message_date).timestamp(),
            "date": local_message_date,
            "ago": utils.pretty_date(parser.parse(local_message_date).astimezone(tz.gettz('UTC')).replace(tzinfo=None)),
            "from": email_from,
        })
    return res
