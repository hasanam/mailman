from zipfile import ZipFile

def pretty_date(time=False):
    """
    Get a datetime object or a int() Epoch timestamp and return a
    pretty string like 'an hour ago', 'Yesterday', '3 months ago',
    'just now', etc
    """
    from datetime import datetime
    now = datetime.utcnow()
    if type(time) is int:
        diff = now - datetime.fromtimestamp(time)
    elif isinstance(time,datetime):
        diff = now - time
    elif not time:
        diff = now - now
    second_diff = diff.seconds
    day_diff = diff.days

    if day_diff < 0:
        return ''

    if day_diff == 0:
        if second_diff < 10:
            return "just now"
        if second_diff < 60:
            return str(second_diff) + " seconds ago"
        if second_diff < 120:
            return "a minute ago"
        if second_diff < 3600:
            return str(round(second_diff / 60, 1)) + " minutes ago"
        if second_diff < 7200:
            return "an hour ago"
        if second_diff < 86400:
            return str(round(second_diff / 3600, 1)) + " hours ago"
    if day_diff == 1:
        return "Yesterday"
    if day_diff < 7:
        return str(round(day_diff, 1)) + " days ago"
    if day_diff < 31:
        return str(round(day_diff / 7, 1)) + " weeks ago"
    if day_diff < 365:
        return str(round(day_diff / 30, 1)) + " months ago"
    return str(round(day_diff / 365, 1)) + " years ago"

def get_imap(domain):
  with ZipFile('imap.zip') as myzip:
    with myzip.open('IMAP.txt') as myfile:
      imaps = myfile.read().decode('UTF-8')
      for line in imaps.splitlines():
        if line.split(":")[0] == domain:
          return (line.split(":")[1], line.split(":")[2])
